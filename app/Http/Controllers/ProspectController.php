<?php

namespace App\Http\Controllers;

use App\Exceptions\ValidationException;
use App\Exceptions\UserRegisterException;
use App\Mail\ContactMail;
use App\Models\Prospect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ProspectController extends Controller
{

    /** 
     *  Create prospect
     *  Method: Post
     *  Parameters: name | lastname | email
    */
    public function create(Request $request)
    {
        try {
            DB::beginTransaction();

            $email = $request->request->get('email') ?? null;
            $name = $request->request->get('name') ?? null;
            $lastname = $request->request->get('lastname') ?? null;

            /** check that the parameters are valid */
            if(empty($email) || empty($name) || empty($lastname)) throw new ValidationException('Parametros invalidos');
            
            /** check if the user is registered */
            $prospectExist = Prospect::where('email',$email)->first();
            if(!is_null($prospectExist)) throw new UserRegisterException('El usuario ya se encuentra registrado');

            /** create user */
            Prospect::create($request->all());
            /** Notify by sending an email */
            Mail::to($email)->send(new ContactMail($name));

            DB::commit();
            return response()->json('Los datos se enviaron correctamente, recibirá un correo a la brevedad', 201);
        } catch (ValidationException $e) {
            DB::rollback();
            return response()->json($e->getMessage(), 400);                    
        } catch (UserRegisterException $e) {
            DB::rollback();
            return response()->json($e->getMessage(), 400);
        } catch (\Throwable $e) {
            DB::rollback();
            return response()->json($e->getMessage(),500);
        }
    }
}
