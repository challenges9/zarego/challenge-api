# API CHALLENGE CABIFY

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Mira **Deployment** para conocer como desplegar el proyecto.


### Información 📋

```
PHP ^7.3|^8.0
LUMEN ^8.3.1
MYSQL
```

### Instalación 🔧
_Clonar_
```
git clone git@gitlab.com:challenges9/zarego/challenge-api.git
cd challenge-api
```

_Configurar variables de entorno y credenciales de base de datos_
```
 cp .env.example .env
```

_Iniciar y actualizar submodulo laradock_
```
git submodule init
git submodule update
```

_Configurar variables de entorno laradock_
```
cd laradock
cp .env.example .env
```

_Levantar contenedores_
```
docker-compose up -d nginx mysql
```

_Configurar DNS_
```
El en archivo /etc/hosts añadir la siguiente linea:
127.0.0.1 api.cabify.dev.com
```

_Instalar dependencias_
```
docker-compose exec workspace bash
composer install
php artisan key:generate
exit
```

_Crear base de datos y ejecutar migración_
```
Ubicarse en la raiz del proyecto: (cd ..)
En MYSQL crear la base de datos con el nombre dado por el .env('DB_NAME')
Ejecutar migración: php artisan migrate
```

_Modificar variables de entorno (.env)_
```
- Remplazar 'localhost' por 'mysql'
DB_HOST=mysql 

- Estas credenciales se enviarán via correo electrónico.
MAIL_USERNAME=*******
MAIL_PASSWORD=*******
MAIL_FROM_ADDRESS=*******

```

_Verificar_
```
En el navegador probar que la ruta 'api.cabify.dev.com' funcione correctamente
```

## Autores ✒️
* **Alex Velasquez**
